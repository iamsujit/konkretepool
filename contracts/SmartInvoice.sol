pragma solidity ^0.5.0;

import "./IERC20.sol";

contract SmartInvoice {

    enum Status { UNCOMMITTED, COMMITTED, BOUGHT, SETTLED }
    function getStatusString(Status status)
    public
    pure
    returns (string memory)
    {
        if (Status.UNCOMMITTED == status) {
            return "UNCOMMITTED";
        }
        if (Status.COMMITTED == status) {
            return "COMMITTED";
        }
        if(Status.BOUGHT == status){
            return "BOUGHT";
        }
        if (Status.SETTLED == status) {
            return "SETTLED";
        }
        return "ERROR";
    }
    uint256 private amount;
    uint256 private totalSupply;
    string public symbol = "INV";
    uint8 public constant DECIMALS = 18;
    uint256 public dueDate;
    uint256 public askingPrice;
    IERC20 public assetToken;
    address public seller;
    address public payer;
    string public referenceHash;
    IERC20 private daiAddr;
    mapping(address => uint256) public balanceOf;
    mapping(address => mapping(address => uint256)) public allowance;

    Status  public status;

    event Transfer(
        address indexed _from,
        address indexed _to,
        uint256 _value
        );
    event Approval(
        address indexed _owner,
        address indexed _spender,
        uint256 _value
        );

    /**
     * @dev Constructor that gives msg.sender all of existing tokens.
     * _amount is the paramenter for to keep the track of tokens
     * _askingPrice is the seller requesting amount
     * _dueDate is the Invoice has to settle before the dueDate or buyer will get penalised
     * _seller the one who creates the smart Invoice
     * _payer the wallet for which the smart invoice is created (Buyer)
     */
    constructor(uint256 _amount,
                uint256 _askingPrice,
                uint256 _dueDate,
                address _seller,
                address _payer,
                IERC20 _daiAddr,
                string memory _referenceHash) public {
        require(_seller != address(0), "seller cannot be 0x0");
        require(_payer != address(0), "payer cannot be 0x0");
        require(_amount > 0, "amount cannot be 0");
        require(_askingPrice < _amount, "asking price cannot be greter than amount");
        require(_seller == msg.sender, "Only seller can create this invoice");
        totalSupply = _amount;
        amount = _amount;
        askingPrice = _askingPrice;
        dueDate = _dueDate;
        seller = _seller;
        payer = _payer;
        referenceHash = _referenceHash;
        daiAddr = _daiAddr;
        status = Status.UNCOMMITTED;
    }


    function changeSeller(address _newSeller) public returns (bool) {
        require(msg.sender == seller, "caller not current seller");
        require(_newSeller != address(0), "new seller cannot be 0x0");
        require(status != Status.SETTLED, "can not change seller after settlement");
        require(status != Status.COMMITTED, "Can not change seller after committed from buyer");
        require(status != Status.BOUGHT, "Can not change seller after buying invoice");
        seller = _newSeller;
        return true;
    }


    function commit() public returns (bool) {
        require(msg.sender == payer, "only payer can commit");
        require(status == Status.UNCOMMITTED, "can only commit while status in UNCOMMITTED");
        status = Status.COMMITTED;
        balanceOf[seller] = totalSupply;
        totalSupply = 0;
        return true;
    }

    // dai initialise with the dai smart contract
    function buyInvoice(uint256 _dai) public returns (bool) {
        require(status != Status.BOUGHT, "already bought");
        require(_dai >= askingPrice,"asking price needs to be available as ether and it should be equal or greated than asking amount");
        require(balanceOf[seller] == amount);
        daiAddr.transferFrom(msg.sender, seller, _dai);
        balanceOf[seller] -= amount;
        balanceOf[msg.sender] += amount;
        emit Transfer(seller, msg.sender, amount);
        status = Status.BOUGHT;
        return true;
    }

    function settle() public payable returns (bool) {
        require(msg.sender == payer, "only payer can settle");
        require(status == Status.BOUGHT, "Not bought by anyone, cannot settle");
        require(status != Status.SETTLED, "already settled");
        require(msg.value >= amount);
        daiAddr.transferFrom(msg.sender, address(this), amount);
        emit Transfer(msg.sender, address(this), amount);
        status = Status.SETTLED;
        return true;
    }

    function redeemInvTokens(uint256 _amount) public returns(bool){
        require(balanceOf[msg.sender] <= amount);
        require(status == Status.SETTLED, "Not settled buy payer");
        balanceOf[msg.sender] -= _amount;
        daiAddr.transferFrom(address(this),msg.sender,_amount);
        return true;
    }
}
