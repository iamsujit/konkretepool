pragma solidity ^0.5.12;

import "./SujiToken.sol";

contract SujiTokenSale{
	address payable admin;
	SujiToken public tokenContract;
	uint256 public tokenPrice;
	uint256 public tokenSold;

	event Sell(address _buyer, uint256 _amount);

	constructor(SujiToken _tokenContract, uint256 _tokenPrice) public {
		// Assign an admin
		admin = msg.sender;
		tokenContract = _tokenContract;
		// Token Price
		tokenPrice = _tokenPrice;
	}
	function multiply(uint x, uint y) internal pure returns(uint z) {
		require(y == 0 || (z = x * y) / y == x);
	}
	// buy token
	function buyToken(uint256 _numberOfTokens) public payable {
		// Require that value is equal to tokens
		require(msg.value == multiply(_numberOfTokens, tokenPrice));
		// require that the contract has enough tokens
		require(tokenContract.balanceOf(address(this)) >= _numberOfTokens);
		require(tokenContract.transfer(msg.sender, _numberOfTokens));
		// require that a transfer successful
		// keep track of token sold
		tokenSold += _numberOfTokens;
		// trigger sell event
		emit Sell(msg.sender, _numberOfTokens);
	}

	function endSale() public {
		// Require admin
		require(msg.sender == admin);
		// transfer the remaining SUJI Tokens to admin
		require(tokenContract.transfer(admin, tokenContract.balanceOf(address(this))));
		// Destroy the contrtact
		selfdestruct(admin);
	}
}
