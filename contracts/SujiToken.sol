pragma solidity 0.5.12;
contract SujiToken {
	string public name = "SujiToken";
	string public symbol = "SUJI";
	string public standard = "Suji Token v1.0";
	uint256 public totalSupply;

	event Transfer(
		address indexed _from,
		address indexed _to,
		uint256 _value
		);
	// approve event
	event Approval(
		address indexed _owner,
		address indexed _spender,
		uint256 _value
		);
	// allowance
	mapping(address => mapping(address => uint256)) public allowance;
	mapping(address => uint256) public balanceOf;

	constructor(uint256 _initialSupply) public{
		balanceOf[msg.sender] = _initialSupply;
		totalSupply = _initialSupply;
	}

	function transfer(address _to, uint256 _value) public returns (bool success){
		// check if the owner is trying tto transfer more tokens than address has
		require(balanceOf[msg.sender] >= _value);
		balanceOf[msg.sender] -= _value;
		balanceOf[_to] += _value;
		emit Transfer(msg.sender,_to,_value);
		return true;
	}
	// these are EPI20 (used for to spend tokens on behalf of owner)
	// approve function
	function approve(address _spender, uint256 _value) public returns (bool success){
		// handle allowance
		allowance[msg.sender][_spender] = _value;
		// handle approve event
		emit Approval(msg.sender,_spender,_value);
		return true;
	}
	// transferFrom function
	function transferFrom(address _from, address _to, uint _value) public returns (bool success) {

		// Require _from has enough tokens
		require(_value <= balanceOf[_from]);
		// Require the allowance is big enough
		require(_value <= allowance[_from][msg.sender]);
		// Change the balance
		balanceOf[_from] -= _value;
		balanceOf[_to] += _value;
		// update the allowance
		allowance[_from][msg.sender] -= _value;
		// Transfer Event
		emit Transfer(_from, _to, _value);
		// return a boolean
		return true;
	}
}
