var SujiToken = artifacts.require("SujiToken");
var SujiTokenSale = artifacts.require("SujiTokenSale");

module.exports = function(deployer) {
	deployer.deploy(SujiToken,1000000).then(function () {
		//token price in ether
		var tokenPrice = 100000000000000;
		return deployer.deploy(SujiTokenSale, SujiToken.address, tokenPrice);
	});
}
